#pragma  once
#include "api.h"
#include <vector>
namespace FL
{

	scope(sync)
	{
		FL_IMPLEMENT_SCOPE(sync);
	public:
		FL_SCOPE_CONSTRUCTOR(sync){
			m_syncing = true;
			m_sync_flow = start_syncme();
		}

		
		void start_sync()
		{
			//TODO: api::flow_notify(m_sync_flow);
			m_syncing = false;

			api::flow_waitlist_clear( );

			auto iter = m_syncs.begin();
			auto iterEnd = m_syncs.end();
			for( ; iter != iterEnd; ++iter)
			{
				if( !iter->expired() )
					iter->lock()->wait_sync();
				
			}
		}

		Flow m_sync_flow;
		flow_decl_0( syncme )
		{
			//api::flow_suspend();
			while( m_syncing)
			{
				api::nop();
			}

			return 0;
		}

		sync wait_sync()
		{
			sync syncback = new_scope(sync);
			m_syncs.push_back( weak_sync( syncback ) );

			api::flow_wait(m_sync_flow);

			return syncback;
		}

	private:
		std::vector<weak_sync>		m_syncs;
		bool				m_syncing;
	};
}
