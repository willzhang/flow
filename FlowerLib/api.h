#pragma  once
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include "Scope.h"
#include "FlowResult.h"
#include "Flow.h"
#include "windows.h"


namespace FL{
	
	typedef boost::function<FlowResult( void )>  FlowFunc;

class api
{
public:
	// flow 
	static FlowResult flow_wait( Flow f);		
	static Flow flow_wait( FlowList fs);
	static FlowResult flow_waitresult(FlowList fs);
	
	static void flow_waitlist_push(Flow f);
	static void flow_waitlist_push(FlowList fs);
	static void flow_waitlist_clear();
	static void flow_waitlist_swap(FlowList & fs);
	static Flow flow_wait_waitlist();
	static bool flow_waitlist_check();




	static void nop();
	static void flow_terminate( Flow f);

	static FlowResult flow_result( Flow f);	
	static bool flow_is_finished( Flow f );

	static void scope_start( Scope s );
	

	// for inner use	
	static Flow _flow_func( base_scope * sc, FlowFunc func, const char * flowname);
	
	// Scope can call a flow only when it's in execute
	// otherwise scope call an empty flow which immediately finish, and return "Scope Not Executing "
	static bool _scope_check_execute( base_scope * sc);
	static void _scope_sign_in_execute( base_scope * sc);
	static void _scope_sign_out_execute( base_scope * sc);

	//

};
}

#define flow_decl_0( flowname)															\
	FL::Flow start_##flowname(){																	\
	return FL::api::_flow_func(this, boost::bind( &self_t::flowname, this), #flowname );		\
}	FL::FlowResult flowname() 


#define  flow_decl_1( flowname, t1)															\
	FL::Flow start_##flowname(t1 p1){																	\
	return FL::api::_flow_func(this, boost::bind( static_cast<FL::FlowResult (self_t::*)(t1) >( &self_t::flowname), this, p1), #flowname ) ;		\
}	FL::FlowResult flowname(t1 p1)

#define  flow_decl_2( flowname, t1, t2)															\
	FL::Flow start_##flowname(t1 p1, t2 p2){																	\
	return FL::api::_flow_func(this, boost::bind( static_cast<FL::FlowResult (self_t::*)(t1, t2) >( &self_t::flowname), this, p1, p2), #flowname ) ;		\
}	FL::FlowResult flowname(t1 p1, t2 p2)

#define  static_flow( flowname )			\
	static FL::Flow start_##flowname(){			\
	return FL::api::_flow_func(NULL, &self_t::flowname , #flowname );	\
}	static FL::FlowResult flowname()			


#define  static_flow_1( flowname, t1)															\
	static FL::Flow start_##flowname(t1 p1){																	\
	return FL::api::_flow_func(NULL, boost::bind(  &self_t::flowname, p1), #flowname ) ;		\
}	static FL::FlowResult flowname(t1 p1)

#define  static_flow_2( flowname, t1, t2)															\
	static FL::Flow start_##flowname(t1 p1, t2 p2){																	\
	return FL::api::_flow_func(NULL,  boost::bind(&self_t::flowname, p1, p2), #flowname ) ;		\
}	static FL::FlowResult flowname(t1 p1, t2 p2)

