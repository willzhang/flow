#pragma once
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include "Flow.h"
namespace FL{
	enum EScopeStatus
	{

	};
	class base_scope
	{
	public:
		base_scope(const char * );
		virtual ~base_scope(void);

		void start()
		{
		}


		
		virtual Flow start_main() { return Flow();}

		std::string		_scope_name;		
	private:
		base_scope(){}
		
	};
	typedef boost::shared_ptr<FL::base_scope> Scope;
}



#define  scope( scope_name )									\
class scope_name##_scope;										\
typedef boost::shared_ptr<scope_name##_scope> scope_name;		\
typedef boost::weak_ptr<scope_name##_scope> weak_##scope_name;		\
class scope_name##_scope : public virtual FL::base_scope

#define scope_derive(scope_name, base_scope_name )		\
class scope_name##_scope;										\
typedef boost::shared_ptr<scope_name##_scope> scope_name;		\
class scope_name##_scope : public base_scope_name

#define  scope_decl( scope_name )								\
class scope_name##_scope;										\
	typedef boost::shared_ptr<scope_name##_scope> scope_name

#define new_scope( scope_name, ...)		scope_name( new scope_name##_scope (  __VA_ARGS__ ) )
	
#define FL_IMPLEMENT_SCOPE( scope_name )		typedef scope_name##_scope self_t
#define FL_SCOPE_CONSTRUCTOR( scope_name, ... )	\
	scope_name##_scope (__VA_ARGS__) :base_scope(#scope_name)
#define FL_SCOPE_DESTRUCTOR(scope_name)				\
	~scope_name##_scope (__VA_ARGS__)