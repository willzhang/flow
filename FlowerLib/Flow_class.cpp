#include "StdAfx.h"
#include "Flow_class.h"
#include "Sandbox_impl.h"

namespace FL{

	extern Sandbox_impl * _cursb();

	Flow_class::Flow_class( const char * name  ):m_status(Execute), _flow_name(name), m_wait_queue()
	{
		if ( _cursb()->get_debug_level() >= 4)
		{
			std::printf("DEBUG: Flow %s created \n", _flow_name.c_str());
		}
		
	}

	Flow_class::~Flow_class( void )
	{
		m_wait_queue.clear();
		if ( _cursb()->get_debug_level() >= 4)
			std::printf("DEBUG: Flow %s destroyed \n", _flow_name.c_str());
	}




	FL::Flow WaitingFlow_class::Wake()
	{
		flow->_waiting.reset(); 
		return flow;
	}

}