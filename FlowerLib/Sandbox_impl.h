#pragma once
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/function.hpp>
#include <hash_map>
#include <list>
#include <vector>
#include <map>

#include "Sandbox.h"
#include "FlowResult.h"
#include "TerminateException.h"
#include "volca.h"




namespace FL{

#pragma warning( disable : 4290 )

typedef boost::weak_ptr<Flow_class> WeakFlow;

class Sandbox_impl : public Sandbox
{
public:
	Sandbox_impl(void);
	virtual ~Sandbox_impl(void);	
public:
	bool update();
	virtual void start (boost::function< FlowResult (HINSTANCE hInstance,
		HINSTANCE hPrevInstance,
		LPTSTR    lpCmdLine,
		int       nCmdShow) >, 

		HINSTANCE hInstance,
		HINSTANCE hPrevInstance,
		LPTSTR    lpCmdLine,
		int       nCmdShow);

	virtual void start ( boost::function< FlowResult (int , _TCHAR * []  ) >,   int argc, _TCHAR * argv[] );	

	void stop();

	void set_debug_level(int i ){ m_debugmode_level = i;}
	int get_debug_level(){ return m_debugmode_level; }

private:
	void copyArgv( int argc, _TCHAR * argv[] );
private:
	// Flow
	FlowResult WaitFlow( Flow f2);
	Flow WaitFlow(FlowList& fl);

	FlowResult WaitResult(FlowList & fl);
	FlowResult GetResult( Flow f);
	
	Flow CallFlowFunc( base_scope *,FL::FlowFunc , const char * fn);	
	Flow PopFlow();
	void updateFlow( Flow flow );
	void DestroyFiber( Flow flow);	
	Flow thisFlow();
	void Return( FlowResult result);
	void Relax( );
	void Terminate(Flow f);

	bool IsFlowFinished( Flow f);
	

	void BackToMainUpdate();

	void CheckTerminate() throw (TerminateException);

	FlowList & __waitlist();

	

private:
	//Scope
	void SignInScope( base_scope *);
	void SignOutScope(base_scope *);
	bool CheckExecuteScope(base_scope *);

private:
	void tryFiber();


	
	


	LPVOID m_mainFiber;

	stdext::hash_map<LPVOID, WeakFlow >	m_flows;

	std::list<Flow>	m_executing_flows;

	std::list<Flow> m_next_flows;

	std::list<Flow> m_finished_flows;

	std::list<Volca> m_volcas;

	stdext::hash_map<base_scope*, std::vector<Flow_class * > >	m_executing_scopes;

	Scope  m_top_scope;

	_TCHAR * m_argbuff;
	_TCHAR ** m_argv;

	
private:


	
	
	int		m_debugmode_level;		// -1 no print 0 - error, 1 - warning,  2 - info, 3 - debug

	
	friend void WINAPI FlowStartProc( LPVOID fc );
	friend class api;
};


}