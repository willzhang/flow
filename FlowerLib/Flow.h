#pragma once
#include <boost/shared_ptr.hpp>
#include <list>
namespace FL{
class Flow_class;
typedef boost::shared_ptr<Flow_class> Flow;
typedef std::list<Flow> FlowList;

}