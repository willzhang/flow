#pragma once
#include "api.h"

namespace FL{

	// 
	
	class Volca
	{
	public:
		Volca(void);
		~Volca(void);

		void AddFlow(Flow & f )
		{
			m_next_flows.push_back(f);
		}
		void Erupt(FlowList & flows)
		{
			flows.splice(flows.end(), m_next_flows);
		}
	private:
		FlowList	m_next_flows;
	};
}
