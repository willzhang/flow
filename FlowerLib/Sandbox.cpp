#include "stdafx.h"
#include "Sandbox.h"
#include "Sandbox_impl.h"
namespace FL{


	Sandbox * CreateSandbox()
	{
		return new Sandbox_impl();
	}
}