#pragma once

#include "api.h"
#include <boost/function.hpp>

namespace FL{
	
	class Sandbox{
	public:
		Sandbox(){}
		virtual ~Sandbox() {} 


		virtual void start (boost::function< FlowResult (HINSTANCE hInstance,
			HINSTANCE hPrevInstance,
			LPTSTR    lpCmdLine,
			int       nCmdShow) >, 
			
			HINSTANCE hInstance,
			HINSTANCE hPrevInstance,
			LPTSTR    lpCmdLine,
			int       nCmdShow)=0;

		virtual void start ( boost::function< FlowResult (int , _TCHAR * []  ) >,   int argc, _TCHAR * argv[] ) = 0;


		virtual bool update() = 0;
		virtual void stop() = 0;
		virtual void set_debug_level(int i) = 0;
		virtual int get_debug_level() = 0;

	};

	Sandbox * CreateSandbox();
}