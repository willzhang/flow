#pragma  once;
#include "../FlowerLib/api.h"

#include <io.h>

#include <conio.h>

namespace FL
{
	scope( system )
	{
		FL_IMPLEMENT_SCOPE(system);
	public:
		FL_SCOPE_CONSTRUCTOR( system )
		{
			stdio_lock = false;
		}
	
		flow_decl_0( readline ){
			lock_stdio l(this);
			
			int buffersize = 100;
			char * buffer = new char [ buffersize ];

			int ichar = 0;
			while( true )
			{
				while( _kbhit() )
				{
					if ( ichar >= buffersize )
					{	

						buffersize *= 2;
						char * newbuffer = new char [buffersize ];
						memcpy(newbuffer, buffer, ichar);						

						delete [] buffer;
						buffer = newbuffer;								
					}

					char c = _getch();
					buffer[ichar] = c;
					
					
					if ( buffer[ichar] == '\r')
					{
						printf("\n");
						buffer[ichar] ='\0';
						std::string ret(buffer);
						
						return ret;
					}

					printf("%c", buffer[ichar] );

					ichar ++;
				}
				api::nop();
			}
	
					
			return 0;
		}

		flow_decl_1( writeline, std::string  )
		{
			lock_stdio l(this);

			printf( "%s\n", p1.c_str() );


			return 0;
		}

	private:
		struct lock_stdio
		{
			self_t * sys;
			lock_stdio( self_t * s)
			{
				sys = s;
				api::flow_wait( sys->start__lock_stdio() );
			}

			~lock_stdio( )
			{
				api::flow_wait( sys->start__unlock_stdio() );
			}

		};
		flow_decl_0( _lock_stdio )
		{
			while ( stdio_lock )
			{
				api::nop();
			}

			stdio_lock = true;

			return 0;
		}

		flow_decl_0( _unlock_stdio )
		{
			assert( stdio_lock );

			stdio_lock = false;

			return 0;
		}

		bool stdio_lock;
	};


}