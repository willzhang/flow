#include "StdAfx.h"
#include "Scope.h"
#include "api.h"

namespace FL{
	base_scope::base_scope( const char * name ): _scope_name(name)
	{
		api::_scope_sign_in_execute( this);
	}

	base_scope::~base_scope(void)
	{
		api::_scope_sign_out_execute(this);		
	}

}