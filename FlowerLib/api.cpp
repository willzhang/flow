#include "StdAfx.h"
#include "api.h"
#include "Sandbox.h"
#include "Scope.h"
#include "Sandbox_impl.h"

#include <boost/assert.hpp>
namespace FL{

	static Sandbox_impl * s_currentSandbox = NULL;
	Sandbox_impl * _cursb()
	{
		assert( s_currentSandbox );
		return s_currentSandbox;
	}

	void _setsb( Sandbox_impl * s )
	{
		if( s ) assert( s_currentSandbox == NULL );
		if( s == NULL ) assert( s_currentSandbox );
		s_currentSandbox = s;
	}

	



	FL::FlowResult api::flow_wait( Flow f )
	{
		return _cursb()->WaitFlow(f);
	}

	FL::Flow api::flow_wait( FlowList fs )
	{
		return _cursb()->WaitFlow(fs);
	}


	FL::Flow api::_flow_func( base_scope * sc, FL::FlowFunc func, const char * fn )
	{
		return _cursb()->CallFlowFunc(sc, func, fn);
	}

	//void api::flow_return( FlowResult result )
	//{
	//	_cursb()->Return( result );
	//}

	void api::nop()
	{
		_cursb()->Relax();
	}

	void api::flow_terminate( Flow f )
	{
		_cursb()->Terminate( f);
	}

	void api::scope_start(Scope s )
	{
			
	}
	bool api::_scope_check_execute( base_scope * sc )
	{
		return _cursb()->CheckExecuteScope(sc);
	}

	void api::_scope_sign_in_execute( base_scope * sc )
	{
		_cursb()->SignInScope(sc);
	}

	void api::_scope_sign_out_execute( base_scope * sc )
	{
		_cursb()->SignOutScope(sc);
	}

	bool api::flow_is_finished( Flow f )
	{
		return _cursb()->IsFlowFinished( f);
	}

	FL::FlowResult api::flow_result( Flow f )
	{
		return _cursb()->GetResult(f);
	}

	FL::FlowResult api::flow_waitresult( FlowList fs )
	{
		return _cursb()->WaitResult(fs);
	}

	void api::flow_waitlist_push( Flow f )
	{
		_cursb()->__waitlist().push_back(f);
	}

	void api::flow_waitlist_push( FlowList fs )
	{
		FlowList & waitlist = _cursb()->__waitlist();
		waitlist.splice(waitlist.end(), fs);
	}

	void api::flow_waitlist_clear()
	{
		_cursb()->__waitlist().clear();
	}

	void api::flow_waitlist_swap( FlowList & fs )
	{
		_cursb()->__waitlist().swap(fs);
	}

	FL::Flow api::flow_wait_waitlist()
	{
		return _cursb()->WaitFlow(_cursb()->__waitlist() );
	}

	bool api::flow_waitlist_check()
	{
		return !_cursb()->__waitlist().empty();
	}





}