#pragma once
#include "api.h"

#include <boost/function.hpp>
#include <list>
#include <boost/weak_ptr.hpp>
namespace FL{
	
	
typedef boost::weak_ptr<Flow_class> WeakFlow;

class WaitingFlow_class
{
public:
	WaitingFlow_class(Flow f ):flow(f)
	{

	}
	~WaitingFlow_class()
	{

	}

	Flow Wake();

private:
	Flow flow;
};
typedef boost::shared_ptr<WaitingFlow_class> WaitingFlow;
typedef boost::weak_ptr<WaitingFlow_class> WeakWaitingFlow;
typedef std::list<WeakWaitingFlow> WeakWaitingFlowList;

class Flow_class
{
public:
	enum Status
	{
		None = -1,
		Execute,
		Finished,
		Terminating
	};
public:
	Flow_class( const char * name );
	~Flow_class(void);

	LPVOID GetFiber()
	{
		return m_fiber;
	}

	FlowResult	GetResult(){
		return m_result;
	}

	Status GetStatus()
	{
		return m_status;
	}


	

	friend class Sandbox_impl;
	// 
	Status m_status;

	// fiber func
	LPVOID m_fiber;
	boost::function<FL::FlowResult( void )>	m_fiberfunc;

	WeakWaitingFlowList			m_wait_queue;

	FlowList			m_waitlist;		//knowing who can awake me

	std::string			_flow_name;


	WaitingFlow			_waiting;

	FlowResult m_result;
};

}