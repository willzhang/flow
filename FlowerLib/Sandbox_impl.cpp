#include "StdAfx.h"
#include "Sandbox_impl.h"

#include "api.h"

#include "Flow_class.h"
#include "TerminateException.h"
#include <boost/bind.hpp>
#include <boost/assert.hpp>
#include <boost/weak_ptr.hpp>

namespace FL{

	extern Sandbox_impl * _cursb();	// in api.cpp
#define __print( category)		
#define error( s, ... )	{  if(m_debugmode_level>=0 ) {printf( "ERROR: "); printf(s, __VA_ARGS__ ); printf("\n"); }  }
#define warning( s, ... )	{  if(m_debugmode_level>=1 ) {printf( "WARNING: "); printf(s, __VA_ARGS__ ); printf("\n"); } }
#define info( s, ... )	{  if(m_debugmode_level>=2 ) {printf( "INFO: "); printf(s, __VA_ARGS__ ); printf("\n"); } }
#define debug( s, ... )	{  if(m_debugmode_level>=3 ) {printf( "DEBUG: "); printf(s, __VA_ARGS__ ); printf("\n"); } }


	void WINAPI FlowStartProc( LPVOID fc )
	{
		Flow_class * f = (Flow_class*) fc;

		try
		{
			_cursb()->Return( f->m_fiberfunc() );
		}
		catch (TerminateException )
		{
			_cursb()->Return( 0 );
		}


		while(true)
			SwitchToFiber( _cursb()->m_mainFiber );
	}

	Flow Sandbox_impl::CallFlowFunc( base_scope * sc, FlowFunc func, const char * fn)
	{
		if (sc == NULL )
		{
			sc = m_top_scope.get();
		}
		Flow_class * f = new Flow_class(fn);	

		Flow fl = Flow( f);

		info( "new flow %s 0x%x, scope %s 0x%x", f->_flow_name.c_str(), f,   sc->_scope_name.c_str(), sc );

		if( m_executing_scopes.find(sc) != m_executing_scopes.end() )
		{
			f->m_fiber = CreateFiber(1024, &FlowStartProc, f);
			f->m_fiberfunc = func;	
			m_flows[f] = fl;

			m_executing_flows.push_back(fl);

			m_executing_scopes[sc].push_back(f);
		}
		else
		{
			fl->m_status = Flow_class::Finished;
			fl->m_result = -1;
			info( "finish flow %s 0x%x", f->_flow_name.c_str(), f);
		}




		return fl;
	}




	Sandbox_impl::Sandbox_impl(void):m_debugmode_level(0)
	{
		m_flows.bucket_size(100);
	}

	Sandbox_impl::~Sandbox_impl(void)
	{
	}

	FlowResult Sandbox_impl::WaitFlow( Flow f2 )
	{
		FlowList fl;
		fl.push_back(f2);
		return WaitFlow( fl )->GetResult();
	}

	FL::Flow Sandbox_impl::WaitFlow( FlowList & fl )
	{
		if ( fl.empty())
		{
			assert( false );
		}

		Flow f1 = thisFlow();		
		f1->_waiting = WaitingFlow( new WaitingFlow_class( f1 ));

		f1->m_waitlist = fl;	


		{			
			FlowList::iterator iterFlow = f1->m_waitlist.begin();
			FlowList::iterator iterEnd = f1->m_waitlist.end();

			
			for ( ; iterFlow != iterEnd ; ++iterFlow )
			{
				Flow f2 = *iterFlow;
				f2->m_wait_queue.push_back( f1->_waiting );

				// check if already has finished
				if ( f2->GetStatus()  == Flow_class::Finished )
				{
					info( "flow %s 0x%x get result from flow %s 0x%x" , f1->_flow_name.c_str(), f1.get(), f2->_flow_name.c_str(), f2.get());

					f1->_waiting.reset();

					f1->m_waitlist.erase(iterFlow);

					return f2;
				}
			}
		}

		BackToMainUpdate();

		{
			FlowList::iterator iterFlow = f1->m_waitlist.begin();
			FlowList::iterator iterEnd = f1->m_waitlist.end();


			for ( ; iterFlow != iterEnd ; ++iterFlow )
			{
				Flow f2 = *iterFlow;			

				if ( f2->GetStatus()  == Flow_class::Finished )
				{
					info( "flow %s 0x%x get result from flow %s 0x%x" , f1->_flow_name.c_str(), f1.get(), f2->_flow_name.c_str(), f2.get());

					f1->_waiting.reset();

					f1->m_waitlist.erase(iterFlow);

					return f2;
				}
			}
		}

		assert( false );

		return Flow();
		
	}

	FL::FlowResult Sandbox_impl::WaitResult( FlowList & fl )
	{
		return WaitFlow(fl)->GetResult();
	}



	Flow Sandbox_impl::PopFlow()
	{
		if ( m_executing_flows.size() > 0 )
		{
			Flow f = m_executing_flows.front();
			m_executing_flows.pop_front();
			return f;
		}

		return Flow();

	}

	void Sandbox_impl::updateFlow( Flow flow )
	{
		if ( flow->GetStatus() == Flow_class::Finished)
		{
			m_finished_flows.push_back(flow);
		}
		else
		{
			SwitchToFiber( flow->GetFiber() );	
		}

	}

	Flow Sandbox_impl::thisFlow()
	{	
		LPVOID fbdata = GetFiberData();
		Flow f ( m_flows.find( fbdata)->second );

		return f;
	}

	void Sandbox_impl::Return( FlowResult result )
	{
		{			
			Flow flow = thisFlow();
			flow->m_result = result;
			flow->m_status = Flow_class::Finished;		


			//iterator should destroy before switch fiber
			WeakWaitingFlowList::iterator iterWaiting = flow->m_wait_queue.begin();
			WeakWaitingFlowList::iterator iterEnd = flow->m_wait_queue.end();

			for (; iterWaiting != iterEnd; ++iterWaiting)
			{
				if( iterWaiting->expired() )
					continue;

				m_executing_flows.push_back( iterWaiting->lock()->Wake() );
			}



			flow->m_wait_queue.clear();

			m_finished_flows.push_back(flow);

			info( "flow %s 0x%x return" , flow->_flow_name.c_str(), flow.get());

		}

		SwitchToFiber(m_mainFiber);
	}

	void Sandbox_impl::Relax()
	{
		m_next_flows.push_back( thisFlow() );
		BackToMainUpdate();
	}


	void Sandbox_impl::Terminate( Flow f )
	{
		// only executing flows can be terminate
		if ( f->m_status != Flow_class::Execute )
		{			
			return;
		}

		f->m_status = Flow_class::Terminating;

		m_executing_flows.push_front(thisFlow());

		if ( thisFlow() == f)
		{

		}
		else
		{
			m_executing_flows.push_front( f );
		}

		BackToMainUpdate();
	}

	void Sandbox_impl::tryFiber()
	{
		PVOID fb = ConvertThreadToFiber(NULL);;
		if (fb == NULL )
		{
			m_mainFiber =GetCurrentFiber();
		}
		else
		{
			m_mainFiber = fb;
		}
	}

	void Sandbox_impl::BackToMainUpdate()
	{
		SwitchToFiber(m_mainFiber);
		CheckTerminate();
	}


	void Sandbox_impl::DestroyFiber( Flow flow )
	{
		if ( flow->m_fiber)
		{
			DeleteFiber( flow->m_fiber);
			flow->m_fiber = NULL;
		}

	}


	extern void _setsb(Sandbox_impl * );
	struct LockFreeSB
	{
		explicit LockFreeSB(Sandbox_impl * s)
		{
			_setsb(s);
		}

		~LockFreeSB(){
			_setsb(NULL);
		}
	};

	bool Sandbox_impl::update()
	{
		// LOCK

		LockFreeSB l(this);


		m_executing_flows.splice(m_executing_flows.end(), m_next_flows);

		{
			Flow flow;
			int cnt = 0;
			while( flow = PopFlow() )
			{
				++cnt;
				updateFlow(flow);					
			}
		}

		while( m_finished_flows.size() > 0 ){
			Flow	flow = m_finished_flows.front();
			m_finished_flows.pop_front();

			DestroyFiber(flow);		
		}

		return m_next_flows.size()>0;
	}

	void Sandbox_impl::start( boost::function< FlowResult (int , _TCHAR * [] ) > f, int argc, _TCHAR * argv[] )
	{
		LockFreeSB l(this);
		tryFiber();

		Scope tops ( new base_scope( "TOP" ));
		SignInScope( tops.get() );

		m_top_scope = tops;

		copyArgv(argc, argv);
		CallFlowFunc( tops.get(), boost::bind<FlowResult>( f, argc, m_argv ), "MAIN" );

	}
	void Sandbox_impl::start (boost::function< FlowResult (HINSTANCE hInstance,
		HINSTANCE hPrevInstance,
		LPTSTR    lpCmdLine,
		int       nCmdShow) > f,

		HINSTANCE hInstance,
		HINSTANCE hPrevInstance,
		LPTSTR    lpCmdLine,
		int       nCmdShow) 
	{
		m_argbuff = 0;
		m_argv = 0;

		LockFreeSB l(this);
		tryFiber();

		Scope tops ( new base_scope("TOP") );
		SignInScope( tops.get() );

		m_top_scope = tops;

		CallFlowFunc( tops.get(), boost::bind<FlowResult>( f, hInstance, hPrevInstance, lpCmdLine, nCmdShow ), "MAIN" );
	}

	void Sandbox_impl::copyArgv( int argc, _TCHAR * argv[] )
	{
		int totallen = 0;

		for ( int i =0; i < argc; ++ i )
		{
			totallen += _tcslen( argv[i] ) + 1;
		}

		m_argv = new _TCHAR* [argc];
		m_argbuff = new _TCHAR[totallen];

		int curpos = 0;
		for( int i = 0; i < argc; ++ i )
		{
			assert( curpos < totallen );
			_tcscpy_s( m_argbuff+ curpos, totallen - curpos, argv[i]);
			m_argv[i] = m_argbuff+curpos;
			curpos += _tcslen(argv[i]) + 1;

		}

		assert( curpos == totallen );
	}

	void DestroySubFibers( WeakWaitingFlowList& flows )
	{
		WeakWaitingFlowList::iterator iter = flows.begin();
		for( iter; iter != flows.end(); ++ iter )
		{
			if( iter->expired() )
				continue;

			Flow f = iter->lock()->Wake();
			if ( f->m_fiber)
			{	
				DeleteFiber(f->m_fiber);
				f->m_fiber = NULL;
			}			
			DestroySubFibers( f->m_wait_queue );
		}
	}

	void DestroySubFibers( FlowList& flows )
	{
		std::list<Flow>::iterator iter = flows.begin();
		for( iter; iter != flows.end(); ++ iter )
		{
			if ( (*iter)->m_fiber)
			{
				DeleteFiber( (*iter)->m_fiber);
				(*iter)->m_fiber = NULL;
			}			
			DestroySubFibers( (*iter)->m_wait_queue );
		}
	}



	void Sandbox_impl::stop()
	{
		LockFreeSB l(this);

		m_executing_scopes.clear();

		DestroySubFibers(m_next_flows);

		if( m_argbuff )
			delete m_argbuff;
		if(	m_argv)
			delete m_argv;
		m_argv = NULL;
		m_argbuff = NULL;
	}

	void Sandbox_impl::SignInScope( base_scope * s)
	{
		if ( ! CheckExecuteScope (s)  )
		{
			m_executing_scopes[s] = std::vector<Flow_class*>();

			info( "sign in scope %s 0x%x " , s->_scope_name.c_str(), s);
		}
	}

	void Sandbox_impl::SignOutScope( base_scope * s)
	{	
		if (!CheckExecuteScope(s))
		{
			return;
		}

		const std::vector<Flow_class *> & flows = m_executing_scopes[s];

		info( "sign out scope %s 0x%x " , s->_scope_name.c_str(), s);
		for( size_t i =0; i < flows.size(); ++ i )
		{		
			if (m_flows.find(flows[i]) != m_flows.end() && ! m_flows[flows[i]].expired())
			{
				Flow f = m_flows[flows[i]].lock();

				Terminate( f );
			}
		}

		m_executing_scopes.erase(s);
	}

	bool Sandbox_impl::CheckExecuteScope( base_scope * sc)
	{
		return m_executing_scopes.find(sc) != m_executing_scopes.end();
	}

	void Sandbox_impl::CheckTerminate() throw (TerminateException)
	{
		Flow f = thisFlow();
		if (f->m_status == Flow_class::Terminating )
		{
			WeakWaitingFlowList::iterator iterWaiting = f->m_wait_queue.begin();
			WeakWaitingFlowList::iterator iterEnd = f->m_wait_queue.end();

			for (; iterWaiting != iterEnd; ++iterWaiting)
			{
				if( iterWaiting->expired() )
					continue;

				m_executing_flows.push_back( iterWaiting->lock()->Wake() );
			}
			info( "terminate flow %s 0x%x " , f->_flow_name.c_str(), f.get());

			throw TerminateException();
		}
	}

	bool Sandbox_impl::IsFlowFinished( Flow f )
	{
		Flow_class * fl = f.get();

		return fl->m_status == Flow_class::Finished;
	}

	FL::FlowResult Sandbox_impl::GetResult( Flow f )
	{
		Flow_class * fl = f.get();
		return fl->m_result;
	}

	FL::FlowList & Sandbox_impl::__waitlist()
	{
		return thisFlow()->m_waitlist;
	}



}