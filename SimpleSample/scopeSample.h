#pragma once
#include "../FlowerLib/api.h"

using namespace FL;
using namespace std;
using namespace boost;

struct Rendzvoo
{
	Rendzvoo()
	{
		updateCount = 0;
	}
	

	int loginCount;
	int login(string name, string pswd)
	{
		loginCount = updateCount + (name.length() + 1) * (pswd.length() + 1);

		return loginCount;
	}
	
	void logout()
	{

	}


	bool handlerIsPending( int h)
	{
		return h > updateCount;
	}

	bool handlerIsSuccess( int h )
	{
		return updateCount >= h  &&  (h % 5 !=0);
	}

	int updateCount;
	void update(){
		updateCount ++;
	}

};
scope ( online )
{
	FL_IMPLEMENT_SCOPE(online);
public:
	FL_SCOPE_CONSTRUCTOR( online )
	{
		m_service = new Rendzvoo();

		m_rendzvooLoop = start_LoopRendzvoo();
	}

	FL_SCOPE_DESTRUCTOR(online)
	{
		api::flow_terminate(m_rendzvooLoop);

		delete( m_service );
	}

	Rendzvoo * m_service;


	Flow m_rendzvooLoop;
	flow_decl_0 ( LoopRendzvoo )
	{
		while(true)
		{
			m_service->update();
			api::nop();
		}
	}

	
	flow_decl_2( login,  string, string )
	{
		if ( m_character )
		{
			return false;
		}

		int handler = m_service->login(p1, p2);

		while( m_service->handlerIsPending(handler) )
		{
			api::nop();
		};

		if ( m_service->handlerIsSuccess(handler ) )
		{

			m_character = new_scope(CharacterService);

			return true;
		}


		return false;	
	}

	flow_decl_0( logout)
	{
		m_service->logout();
		
		if ( m_character)
		{
			api::_scope_sign_out_execute( m_character.get() );

			m_character.reset();

			api::nop();
		}

		return any();
	}

	scope( LocalCacheService )
	{

	};

	
	

	scope( CharacterService )
	{
		FL_IMPLEMENT_SCOPE(CharacterService);
	public:
		FL_SCOPE_CONSTRUCTOR(CharacterService)
		{

		}

		scope( LobbyService )
		{
			FL_IMPLEMENT_SCOPE(LobbyService);
		public:
			FL_SCOPE_CONSTRUCTOR(LobbyService)
			{

			}
		};

		scope( GeoService)
		{
			FL_IMPLEMENT_SCOPE(GeoService);
		public:
			FL_SCOPE_CONSTRUCTOR(GeoService)
			{

			}
		};

		
		GeoService			m_geo;
		LobbyService		m_lobby;
	};

	CharacterService		m_character;
	LocalCacheService		m_localcache;


	
};


scope(scopeSample )
{
FL_IMPLEMENT_SCOPE( scopeSample );
public:
	FL_SCOPE_CONSTRUCTOR(scopeSample)
	{


	}


	flow_decl_0( sampleOnline )
	{
		online m_online = new_scope(online);
		any res;




		res = api::flow_wait( m_online->start_login("test", "test" ) );

		bool loginSuccess =false;
		if (any_cast<bool>(&res) )
		{
			loginSuccess = any_cast<bool>(res);
		}


		if ( loginSuccess )
		{


		}

		return any();

	}
};
