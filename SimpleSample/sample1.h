#include "../FlowerLib/api.h"

#include <conio.h>
using namespace FL;
using namespace boost;

scope(BasicSample)
{
	FL_IMPLEMENT_SCOPE(BasicSample );
public:
	FL_SCOPE_CONSTRUCTOR( BasicSample )
	{
		
	}


	flow_decl_0( sampleWaiting ){
		::system("cls\n");
		printf(
			"* --------------------------------------------------------------------\n"
			"*		[BasicSample] Sample: Waiting a flow \n"
			"* --------------------------------------------------------------------\n"
			"	Waiting a flow is the basic function for Scope Flow Language. \n"
			"	Flow itself is non-blocking\n "
			"	But you can wait it to make it synchronized! \n"
			"	More, NOT blocking!!!  \n"
			"	Flows that doesn't wait other flow will still be alive. \n"
			"	You need only api::flow_wait \n\n"
			"	SimpleTask(x) is a predefined flow to calculate sum of 0 to x \n"
			"	step by step, one step each frame/update .\n"
			"	You'll find it through all the examples.  \n\n"

			"	Let's run the following code to see what will SimpleTask do. \n"
			);		
		printf("<code> \n");
		printf("	api::flow_wait( SimpleTask( 20 ) );\n");
		printf("</code> \n");
		
		api::flow_wait( start_waitPressAnyKey( "Press any key to run the sample and see the result.") );
		
		api::flow_wait( start_SimpleTask( 20 ) );

		printf("[BasicSample] SimpleTask finishes, and this flow can continue. \n\n\n");
		printf("* --------------------------------------------------------------------\n\n\n");
		printf(
			"[BasicSample] Now let's get result from another SimpleTask.  \n"
			"	And print the result.\n\n");
		printf("<code>\n"
			"	any res = api::flow_wait( SimpleTask( 10 ) );\n"			
			"	if ( any_cast<int>(&res) )		\n"
			"	{\n"
			"		printf(\"result sum of SimpleTask = %%d\\n\", any_cast<int>(res) );\n"
			"	}\n"
			"</code>\n"
			);
		api::flow_wait( start_waitPressAnyKey( "Press any key to run the sample code and see the result. ") );
		any res = api::flow_wait( start_SimpleTask( 10 ) );		
		if ( any_cast<int>(&res) )		
		{
			printf("	Result sum of SimpleTask = %d\n", any_cast<int>(res) );
		}


		printf(
			"[BasicSample] You get the result and printed it. \n"
			"	!!!  IMPORTANT  !!!\n"
			"	You may have realized any_cast.  Yes, Flow can returns any thing!!! \n"
			"	However, it's your responsibility to make sure you cast it correctly \n"
			"	Flow maybe finishes abnormally, e.g. by terminate, or return different\n"
			"	type in different branch.  You can't assert the return type.\n"
			"	So always make sure you can safely cast an any value. \n\n"
			);

		
		printf(	"* --------------------------------------------------------------------\n");
		printf( "*		[BasicSample] This sample finishes. \n");
		printf(	"* --------------------------------------------------------------------\n");
		printf( "\n" );
		
		return any();
	}

	flow_decl_0( sampleMultiFlows ){
		::system("cls\n");
		printf(
			"* --------------------------------------------------------------------\n"
			"*		[BasicSample] Sample: simultaneous flows \n"
			"* --------------------------------------------------------------------\n"
			"	Flows are non-blocking, so what if more flows are running?\n"
			"	Yes, they run simultaneously.\n"
			"	\n"
			);

		printf( "<code>\n"
		"	Flow f1 = SimpleTask( 5 );\n"
		"	Flow f2 = SimpleTask( 30 );\n"
		"	Flow f3 = SimpleTask( 9 );\n"
		"	for (int i = 0; i < 20; ++i )\n"
		"	{	\n"
		"		api::nop();\n"
		"	}\n"
		"</code>\n\n"
		);

		printf("\n");
		printf("[BasicSample] After 20 frames,  \n");
		printf(
			"[BasicSample] You'll find f2 runs over this line \n"
			"	This means f2 is alive\n"
			"	Then relax 5 frames to see f2 is still running for 5 frames.\n"
			);

		printf(
			"<code>\n"
			"	for(int i =0; i < 5; ++ i)	\n"
			"		api::nop();		\n"
			"	printf(\"after relax 5 frames\\n\");\n"
			"	any res = api::flow_wait(f2);\n"
			"	if( any_cast<int>(&res) )\n"
			"		printf( \"result from f2 is %%d\\n\", any_cast<int>(res));\n"
			
			"</code>\n"
			);

		 waitPressAnyKey( "Press any key to run the sample ") ;

		Flow f1 = start_SimpleTask( 5 );
		Flow f2 = start_SimpleTask( 30 );
		Flow f3 = start_SimpleTask( 9 );

		printf("frame");
		for (int i = 0; i < 20; ++i )
		{
			api::nop();
			printf(" %d, ", i );
		}

		for(int i =0; i < 5; ++ i)
			api::nop();	
		printf("after relax 5 frames\n");
		any res = api::flow_wait(f2);
		if( any_cast<int>(&res) )
			printf( "result from f2 is %d\n", any_cast<int>(res));



		waitPressAnyKey( "Press any key to continue the example");

		printf(
			"[BasicSampe] Still you can get result from f1 and f3\n "
			"	just use wait syntax.  \n"
			"	you can even wait one flow several times \n"
			"	that will always return same result\n"
			"	let's try it\n\n"
			);

		printf(
			"<code>\n"
			"	res = api::flow_wait( f1 );\n"
			"	if( any_cast<int>(&res) )\n"
			"		printf( \"result from f1 is %%d\\n\", any_cast<int>(res));	\n"
			"	any res2 = api::flow_wait( f1 );	\n"
			"	if( any_cast<int>(&res) )			\n"
			"		printf( \"again result from f1 is %%d\\n\", any_cast<int>(res2));	\n"
			"	res = api::flow_wait( f3 );	\n"
			"	if( any_cast<int>(&res) )	\n"
			"		printf( \"result from f3 is %%d\\n\", any_cast<int>(res));	\n"
			"	res = api::flow_wait( f2 );\n"
			"	if( any_cast<int>(&res) )\n"
			"		printf( \"result from f2 is %%d\\n\", any_cast<int>(res));\n"
			"</code>\n\n"
		);
		waitPressAnyKey( "Press any key to run the code") ;
		res = api::flow_wait( f1 );
		if( any_cast<int>(&res) )
			printf( "result from f1 is %d\n", any_cast<int>(res));
		any res2 = api::flow_wait( f1 );
		if( any_cast<int>(&res) )
			printf( "again result from f1 is %d\n", any_cast<int>(res2));
		res = api::flow_wait( f3 );
		if( any_cast<int>(&res) )
			printf( "result from f3 is %d\n", any_cast<int>(res));
		res = api::flow_wait( f2 );
		if( any_cast<int>(&res) )
			printf( "result from f2 is %d\n", any_cast<int>(res));
		
		return any();
	}

	flow_decl_0( sampleComplicateWait )
	{
		::system("cls");
		printf(
			"* --------------------------------------------------------------------\n"
			"*		[BasicSample] Sample: Complicate wait flows \n"
			"* --------------------------------------------------------------------\n"
			"<code>\n"
			"	api::flow_wait( StartLoadResource() );\n"
			"	Flow ui = UIInit();\n"
			"	Flow anim = AnimInit();\n"
			"	api::flow_wait(ui);\n"
			"	api::flow_wait(anim);\n"
			"</code>\n"
			"	StartLoadResource will start LoadUIReosurce and LoadAnimationData, \n "
			"	and however, never wait them\n"
			"	UIInit will wait flow_load_ui which is assigned by LoadUIResource. \n"
			"	AnimInit will wait flow_load_anim which is assigned by LoadAnimData. \n"
			"	Flow_load_ui and flow_load_anim are member variables of this scope. \n"
			"	So in this sample, you see how flow cooperate with each other.\n"

			);

		api::flow_wait( start_waitPressAnyKey( "Press any key to run the code") );

		api::flow_wait( start_StartLoadResource() );
		Flow ui = start_UIInit();
		Flow anim = start_AnimInit();
		api::flow_wait(ui);
		api::flow_wait(anim);

		return any();		
	}

	Flow flow_loadResource;
	flow_decl_0( StartLoadResource )
	{
		printf( " StartLoadResource \n" );
		flow_load_ui = start_LoadUIResource();
		flow_load_anim = start_LoadAnimationData();

		printf( " StartLoadResource won't wait   \n" );
		return any();		
	}

	flow_decl_0( UIInit)
	{
		printf( " UIInit wait 13 \n" );
		api::flow_wait(flow_load_ui);

		printf( " UIInit do 3 \n" );
		api::flow_wait( start_SimpleTask(3));

		return any();		
	}

	flow_decl_0( AnimInit)
	{
		printf( " AnimInit wait 11 and 12 \n" );
		api::flow_wait(flow_load_anim);

		printf( " AnimInit do 3 \n" );
		api::flow_wait( start_SimpleTask(4));

		return any();		
	}

	Flow flow_load_ui;
	flow_decl_0( LoadUIResource )
	{
		printf( " LoadUIResource 13 \n" );
		api::flow_wait( start_SimpleTask(13) );
		return any();		
	}


	Flow flow_load_anim;
	flow_decl_0( LoadAnimationData )
	{
		printf( " LoadAnimationData 11 \n" );
		api::flow_wait(start_SimpleTask(11));

		printf( " LoadAnimationData 12 \n" );
		api::flow_wait(start_SimpleTask(12));
		return any();		
	}

	flow_decl_0(sampleMainLoop )
	{
		::system("cls");
		printf(
			"* --------------------------------------------------------------------\n"
			"*		[BasicSample] Sample: Main Loop \n"
			"* --------------------------------------------------------------------\n"
			"	Most engine need to update per frame to make it alive.  \n"
			"	Flow system can also do that.  "
			);

		printf(
		"<code>\n"
		"	flow_decl_1( MainLoop, int & )	\n"
		"	{\n"
		"		while( true )\n"
		"		{\n"
		"			++p1;\n"
		"			api::nop();\n"
		"		}\n"
		"	}\n\n"

		"	int x = 0;	\n"
		"	Flow mainloop = MainLoop(x);\n"
		"	char c = '\\0';\n"
		"	do{\n"
		"		c = '\\0';	\n"
		"		any res = api::flow_wait(waitPressAnyKey() );		\n"
		"		printf(\"Now x is updated to %%d\\n\", x );\n"
		"		if ( any_cast<char>( &res))			\n"
		"		{			\n"
		"			c = any_cast<char>(res);	\n"
		"		}\n"
		"	}while ( c && c!= '\\r');\n"
		"\n"
		"api::flow_terminate(mainloop);\n"
		"</code>\n"
		);

		api::flow_wait( start_waitPressAnyKey( "Press any key to run the code") );

		int x = 0;
		Flow mainloop = start_MainLoop(&x);		

		char c = '\0';
		do{
			c = '\0';
			any res = api::flow_wait(start_waitPressAnyKey("Press any key to view currently updated\n Press enter key to stop this example"));
			printf("Now x is updated to %d\n", x );
			if ( any_cast<char>( &res))
			{
				c = any_cast<char>(res);
			}
		}while ( c && c!= '\r');

		api::flow_terminate(mainloop);

		return any();
	}

	flow_decl_1( MainLoop, int *  )
	{
		while( true )
		{
			++(*p1);
			api::nop();
		}

		return any();
	}

	flow_decl_1( SimpleTask, int )
	{
		int sum = 0;
		for ( int i = 0; i <= p1; ++ i)
		{
			printf( "	SimpleTask is doing %d/%d sum = %d\n", i, p1, sum+=i);

			api::nop();
		}

		return any(sum);
	}

	

	flow_decl_0( waitPressAnyKey ){

		printf( "Press any key to continue ... \n");
		while( !_kbhit() )
		{
			api::nop();
		}

		char c = 0;
		while(_kbhit() ){
			c=_getch();
			api::nop();
		}

		return any(c);
	}
	flow_decl_1( waitPressAnyKey, std::string ){

		printf( "%s\n", p1.c_str());
		while( !_kbhit() )
		{
			api::nop();
		}

		char c = 0;
		while(_kbhit() ){
			c = _getch();
			api::nop();
		}

		return any(c);
	}


	
};

scope( selectSample )
{
	FL_IMPLEMENT_SCOPE( selectSample );
public:
	static_flow_2( select, Flow, Flow )
	{
		while(true )
		{
			if ( api::flow_is_finished(p1) )
			{
				return p1;
			}

			if (api::flow_is_finished(p2))
			{
				return p2;
			}

			api::nop();
		}
	}
};

scope( coroutine )
{
	FL_IMPLEMENT_SCOPE(coroutine);
public:
	


};
