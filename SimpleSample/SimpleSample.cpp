// SimpleSample.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


#include "sample1.h"
#include "../FlowerLib/Sandbox.h"

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

//#include "../FlowerLib/system_scope.h"

#include "scopeSample.h"
FL::FlowResult _tmainflow(int argc, _TCHAR * argv[] )
{
	//scopeSample scpSample = new_scope(scopeSample);

	//api::flow_wait(scpSample->sampleOnline());

	
	BasicSample sample = new_scope(BasicSample );

	api::flow_wait(sample-> start_sampleMainLoop());
	api::flow_wait(sample-> start_waitPressAnyKey( "Press any key to view next sample.  Thanks."));
	
	api::flow_wait( sample-> start_sampleWaiting() );
	api::flow_wait(sample-> start_waitPressAnyKey( "Press any key to view next sample.  Thanks."));
	
	api::flow_wait( sample-> start_sampleMultiFlows());
	api::flow_wait(sample-> start_waitPressAnyKey( "Press any key to view next sample.  Thanks."));

	api::flow_wait(sample-> start_sampleComplicateWait());
	api::flow_wait(sample-> start_waitPressAnyKey( "Press any key to view next sample.  Thanks."));

	any res = api::flow_wait(selectSample_scope::start_select( sample-> start_SimpleTask(20), sample-> start_SimpleTask(30) ) );

	Flow f = any_cast<Flow> (res);

	res = api::flow_result(f);

	printf("wait one of flow result = %d\n", any_cast<int> (res)) ;

	api::flow_wait( sample->start_waitPressAnyKey() );

	return 0;
}


int _tmain(int argc, _TCHAR* argv[])
{
	FL::Sandbox * sb = FL::CreateSandbox();
	sb->start(_tmainflow, argc, argv );
	sb->set_debug_level(4);
			
	while(true )
	{
		sb->update();
	}


	sb->stop();
	return 0;
}



