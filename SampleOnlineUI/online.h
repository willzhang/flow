#pragma  once;
#include "../FlowerLib/api.h"



#include "online_character.h"

namespace Online{
using namespace std;
using namespace FL;
struct Remote
{
	string m_name;
	string m_pswd;

	Remote():logout_timer(0){}
	int logout_timer;
	int login( string name, string pswd)
	{
		logout_timer = 999;
		m_name = name;

		return 10;
	}

	int check_handler(int handler)
	{
		handler --;
		return handler;
	}

	bool is_logged_in()
	{
		return logout_timer > 0;
	}
	void update()
	{
		printf("update remote\n");
		logout_timer --;
	}
};
scope( online )
{
public:
	FL_IMPLEMENT_SCOPE(online);

	FL_SCOPE_CONSTRUCTOR(online)
	{

	}

	Remote remote;

	
	string m_login_name ;
	flow_decl_2( login, string, string ){
		int handler = remote.login(p1, p2);

		
		while( handler )
		{
			handler = remote.check_handler(handler);
			printf("I'm waiting login ... \n");
		}
		

		m_login_name = p1;

		m_character = new_scope( character );

		return 0;
	}

	flow_decl_0( logout )
	{

		return 0;
	}

	flow_decl_0( main )
	{
		while(true)
		{
			remote.update()		;	
		}

		return 0;
	}

	

	character m_character;


};

}