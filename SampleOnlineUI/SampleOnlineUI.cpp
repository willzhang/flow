// SampleOnlineUI.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "../FlowerLib/Sandbox.h"
#include "online.h"

using namespace FL;
FL::FlowResult _tmainflow(int argc, _TCHAR * argv[] )
{
	
	Online::online online = new_scope(Online::online);

	online->main();


	char name[245];
	/*printf("input your name");
	scanf("%s", name );*/

	api::flow_wait( online->start_login(name,name) );


	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	FL::Sandbox * sb = FL::CreateSandbox();
	sb->set_debug_level(4);
	sb->start(_tmainflow, argc, argv );

	while(true )
	{
		sb->update();
	}


	sb->stop();
	return 0;
}

