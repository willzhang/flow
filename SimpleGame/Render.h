#pragma once
#include <d3d9.h>

#include "FlowerLib/sync.h"
scope(Render)
{
// Our custom FVF, which describes our custom vertex structure
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZRHW|D3DFVF_DIFFUSE)

	FL_IMPLEMENT_SCOPE(Render);
public:
	FL_SCOPE_CONSTRUCTOR(Render, HWND hwnd)
		, pD3D(NULL)
		, pd3dDevice(NULL)
		, pVB(NULL)
		, hWnd(hwnd)
		, m_sync(new_scope(sync))
	{
	}

	flow_decl_0(main)
	{
		    // Initialize Direct3D
		if( FAILED( InitD3D( ) ) )
			return 0;
			// Create the vertex buffer
		if( FAILED( InitVB() ) )
			return 0;
				
		while(true)
		{
			m_sync->start_sync();
			
			Render();				
			
			m_sync = new_scope(sync);

			api::nop();
		}


		return 0;
	}

	sync sync_frame()
	{
		while( !m_sync )
			api::nop();

		return m_sync->wait_sync();
	}

private:


private:
	HRESULT InitVB();
	HRESULT InitD3D( );
	void Render();
	LPDIRECT3D9             pD3D ; // Used to create the D3DDevice
	LPDIRECT3DDEVICE9       pd3dDevice; // Our rendering device
	LPDIRECT3DVERTEXBUFFER9 pVB ; // Buffer to hold Vertices
	HWND					hWnd;

	// A structure for our custom vertex type
	struct CUSTOMVERTEX
	{
		FLOAT x, y, z, rhw; // The transformed position for the vertex
		DWORD color;        // The vertex color
	};

	sync m_sync;

};

