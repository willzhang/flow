#pragma once

#include "resource.h"

#define MAX_LOADSTRING 100






scope(MainWindow)
{

	FL_IMPLEMENT_SCOPE(MainWindow);
public:
	FL_SCOPE_CONSTRUCTOR(MainWindow, HINSTANCE inst) 
		, hInstance(inst)
		,m_initialized(false)
	{

	}

private:
	HINSTANCE hInstance;	
	HACCEL		hAccelTable;

public:

	bool m_initialized;
	Flow m_initializing_flow;

	bool WaitInitialize()
	{
		if( m_initialized )
			return true;

		while( ! m_initializing_flow )
		{
			api::nop();
		}

		api::flow_wait(m_initializing_flow);

		return m_initialized;
	}


	HWND GetHWND()
	{
		return hWnd;
	}

	flow_decl_0( init )
	{
		// Initialize global strings
		LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
		LoadString(hInstance, IDC_SIMPLEGAME, szWindowClass, MAX_LOADSTRING);
		MyRegisterClass(hInstance);

		// Perform application initialization:
		if (!InitInstance (hInstance))
		{
			m_initialized = false;		
			return false;
		}

		hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SIMPLEGAME));		
		m_initialized = true;		
		return true;
	}


	flow_decl_0(main)
	{
		m_initializing_flow = start_init();
		api::flow_wait(m_initializing_flow );


		MSG msg;
		while (true)
		{
			GetMessage(&msg, NULL, 0, 0);			

			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			if( msg.message == WM_QUIT)
				break;

			api::nop();
		}


		return FlowResult();
	}

private:
	HWND hWnd;
	// Global Variables:
	HINSTANCE hInst;								// current instance
	TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
	TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

	//
	//   FUNCTION: InitInstance(HINSTANCE, int)
	//
	//   PURPOSE: Saves instance handle and creates main window
	//
	//   COMMENTS:
	//
	//        In this function, we save the instance handle in a global variable and
	//        create and display the main program window.
	//
	BOOL InitInstance(HINSTANCE hInstance)
	{


		hInst = hInstance; // Store instance handle in our global variable

		hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

		if (!hWnd)
		{
			return FALSE;
		}

		ShowWindow(hWnd, SW_SHOWDEFAULT);
		UpdateWindow(hWnd);

		return TRUE;
	}

	//
	//  FUNCTION: MyRegisterClass()
	//
	//  PURPOSE: Registers the window class.
	//
	//  COMMENTS:
	//
	//    This function and its usage are only necessary if you want this code
	//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
	//    function that was added to Windows 95. It is important to call this function
	//    so that the application will get 'well formed' small icons associated
	//    with it.
	//
	ATOM MyRegisterClass(HINSTANCE hInstance)
	{
		WNDCLASSEX wcex;

		wcex.cbSize = sizeof(WNDCLASSEX);

		wcex.style			= CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc	= WndProc;
		wcex.cbClsExtra		= 0;
		wcex.cbWndExtra		= 0;
		wcex.hInstance		= hInstance;
		wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SIMPLEGAME));
		wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
		wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
		wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_SIMPLEGAME);
		wcex.lpszClassName	= szWindowClass;
		wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

		return RegisterClassEx(&wcex);
	}


	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		int wmId, wmEvent;

		switch (message)
		{
		case WM_COMMAND:
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			// Parse the menu selections:
			switch (wmId)
			{
			case IDM_EXIT:
				DestroyWindow(hWnd);
				break;
			default:
				return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		return 0;
	}
};