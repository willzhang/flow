#pragma once

#include "MainWindow.h"
#include "Render.h"
#include "Simulation.h"
scope(Game)
{
	FL_IMPLEMENT_SCOPE(Game);

public:
	FL_SCOPE_CONSTRUCTOR(Game, 
		HINSTANCE inst) , hInstance(inst)
	{

	}

private:
	HINSTANCE hInstance;

public:
	flow_decl_0(main)
	{
		m_mainWindow = new_scope(MainWindow, hInstance);
		Flow mainWindowFlow = m_mainWindow->start_main();		
		m_mainWindow->WaitInitialize();

		m_render = new_scope(Render, m_mainWindow->GetHWND() );
		Flow renderFlow = m_render->start_main();

		api::flow_wait( mainWindowFlow ) ; // TODO: replace by start mainWindow 
		return FlowResult();
	}


	MainWindow m_mainWindow;
	Render m_render;
	Simulation m_simulation;
};

