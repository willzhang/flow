// SimpleGame.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "SimpleGame.h"

#include "FlowerLib/api.h"
#include "FlowerLib/Sandbox.h"

#include <d3d9.h>

using namespace FL;
using namespace boost;



#include "Game.h"

FL::FlowResult   _tmainWinflow(HINSTANCE hInstance,
							   HINSTANCE hPrevInstance,
							   LPTSTR    lpCmdLine,
							   int       nCmdShow) 
{


	Game game = new_scope(Game, hInstance);
	api::flow_wait( game->start_main() ) ; // TODO: replace by start mainWindow 



	return FL::FlowResult();

}





int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	FL::Sandbox * sb = FL::CreateSandbox();
	sb->start(_tmainWinflow, hInstance , hPrevInstance, lpCmdLine, nCmdShow );
	sb->set_debug_level(2);
	
	while (sb->update())
	{
		
	}

	sb->stop();

	return 0;
}








// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
